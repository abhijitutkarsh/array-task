function variable(arr)
{
    var max=0;
    var count;
    var array=[];
    var i,j;
    var maxele;
    for(i=0;i<arr.length;i++)
    {
        count=1;
        for(j=i+1;j<arr.length;j++)
        {
            if(arr[i]===arr[j])
            {
                count+=1;
            }
        }
        if(count>max)
        {
            max=count;
            maxele=arr[i];
        }
    }
    array=[maxele,max];
    return(array);
}
module.exports=variable;
